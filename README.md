# theories/philosophies-top

## About this project

If you have any new idea, this is the right place.

Add new **issue** with label *idea* (title formated as **name of request [idea]**). The maintainer will create a separated repository for your request.

You can find the complete list of projects in group **philosophies** [here](https://gitlab.com/theories/philosophies).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Licenses

Any project content in this project is under [**Creative Commons LICENSE**](LICENSE.txt) and any project code content in this project is under [**MIT LICENSE**](LICENSE-CODE.txt).